$(document).ready(function(){
  validator.init();    
});

var validator = function() {
    return {
        orientationValid : 'before',
        orientationError : 'before',
        validMessages    : true,       
        errorNumeric     : 'Numbers only.',
        errorEmail       : 'Invalid email.',
    
        /**
        * Initialize
        */  
        init : function() {
          // settings
          var settings = Drupal.settings.jsv;
          
          $.each(settings, function(i, e){
            validator[i] = e;
          });               
          // event handlers
          $('.numeric').change(function(){ validator.numeric(this); });
          $('.email').change(function(){ validator.email(this); });
          $('.request').change(function(){ validator.request(this); });
        },
        
        /**
        * Display a valid message
        */ 
        validDisplay : function(input, message) {
          // remove previous messages
          validator.validRemove(input);
          validator.errorRemove(input);
          if (validator.validMessages == true && typeof message != 'undefined'){
            // add messages    
            switch(validator.orientationValid){
              case 'before':
                $(input).before('<span class="validator-valid">' + message + '</span>'); 
                break;
              
              case 'after':
                $(input).after('<span class="validator-valid">' + message + '</span>');
                break;  
            }    
          }
          $(input).addClass('validator-valid-input');
        },
        
        /**
        * Removes a valid message
        */         
        validRemove : function(input) {
          switch(validator.orientationValid){
            case 'before':     
              $(input).prev('.validator-valid').remove(); 
              break;
            
            case 'after':  
              $(input).next('.validator-valid').remove();
              break;  
          } 
          $(input).removeClass('validator-valid-input');
        },
                        
        /**
        * Display a error message
        */ 
        errorDisplay : function(input, message) {
          // remove previous messages
          validator.validRemove(input);
          validator.errorRemove(input);
          // add message
          switch(validator.orientationError){
            case 'before':      
              $(input).before('<span class="validator-error">' + message + '</span>'); 
              break;
            
            case 'after':
              $(input).after('<span class="validator-error">' + message + '</span>');
              break;  
          }          
          $(input).addClass('validator-error-input');
          $(input).removeClass('validator-valid-input');
        },
        
        /**
        * Removes error message
        */         
        errorRemove : function(input) {
          switch(validator.orientationError){
            case 'before':   
              $(input).prev('.validator-error').remove(); 
              break;
            
            case 'after':   
              $(input).next('.validator-error').remove();
              break;  
          }         
          $(input).removeClass('validator-error-input');
        },
              
        /**
        * Numeric
        */         
        numeric : function(input) {
          var value = $(input).val(); 
          
          if (isNaN(value)){
            // add error
            validator.errorDisplay(input, validator.errorNumeric);    
          } 
          else{
            // valid message
            validator.validDisplay(input);
          }
        },
        
        /**
        * Email
        */         
        email : function(input) {
          var value  = $(input).val(); 
          
          if (!validator.emailValidate(value)){
            // add error
            validator.errorDisplay(input, validator.errorEmail);    
          } 
          else{
            // valid message
            validator.validDisplay(input);
          }
        },
        
        /**
        * Email validation
        */          
        emailValidate : function(value) {
          var pieces = value.match("^(.+)@(.+)$");
          
          if (pieces == null){
            return false;
          }
          
          // user 
          if (pieces[1] != null){
            var regexp_user = /^\"?[\w-_\.]*\"?$/;
            if (pieces[1].match("") == null){
              return false; 
            } 
          }
          
          // domain
          if (pieces[2] != null){
            var regexp_domain = /^[\w-\.]*\.[A-Za-z]{2,4}$/;
            if (pieces[2].match(regexp_domain) == null){
              return false;
            }  
          }
          
          return true;        
        },
        
        /**
        * Request
        */         
        request : function(input){
          var value   = $(input).val();
          // TODO: do not use a custom attribute
          var validatorPath = $(input).attr('validator-path'); 
                               
          if (validatorPath){
            $.getJSON(validatorPath + '/' + value, function(json){
              if (json.valid){  
                validator.validDisplay(input, json.message);
              }
              else{
                validator.errorDisplay(input, json.message);
              }
            });          
          }
        }       
    };
}();

