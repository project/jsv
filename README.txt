
     
      JavaScript Validator ( JSV )
      Provided by www.vision-media.ca        
     
      ------------------------------------------------------------------------------- 
      INSTALLATION
      ------------------------------------------------------------------------------- 
      
      To install this module simply enable it. This module does nothing on its own
      but provide an API of sorts to dynamically validate form fields.
      
      ------------------------------------------------------------------------------- 
      VALIDATION TYPES
      ------------------------------------------------------------------------------- 
       
      numeric
      email
      request
        javascript request callback to any path providing the fields value, expects 
        a javscript object 'valid' boolean and an optional 'message' to return when 
        invaild.
        
      -------------------------------------------------------------------------------- 
      HOW TO USE
      ------------------------------------------------------------------------------- 
      
        VALIDATION TYPES
        To user numeric or email validation types, simply add the class 'numeric' or 
        'email' onto the form element and if the validator.js is included in the page 
        it will automatically check.
        
        The request validation type is more involved, view the jsv module page
        on drupal.org/project/jsv to view the documentation.
        
        You will often need to user the FORM_ALTER HOOK

             
      ------------------------------------------------------------------------------- 
      VERSION CHANGES
      ------------------------------------------------------------------------------- 
      
      README.txt added. 
      jsv_settings theme.             
                    
      ------------------------------------------------------------------------------- 
      FUTURE GOALS
      ------------------------------------------------------------------------------- 
      
      To contribute ideas to the future of this module please post a feature request
      on drupal.org.
      
      Future goals include:
        Refactor exsisting code.
        Valid message for requests aswell.  
        More validation types.
          min
          max
          range
          format masks
          


